"""
Decorators and common functions
"""

from functools import wraps
from flask import request, Response


def check_login(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    return username == 'admin' and password == 'admin'

def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
        'Could not verify your access level for that URL.\n'
        'You have to login with proper credentials', 401,
        {'WWW-Authenticate': 'Basic realm="Login Required"'})

def login_required(func_):
    @wraps(func_)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_login(auth.username, auth.password):
            return authenticate()
        return func_(*args, **kwargs)
    return decorated
