
from flask import render_template
from flask import request
from flask import redirect
from flask import url_for
from flask_wtf import Form
from wtforms.validators import DataRequired
from wtforms import StringField
from flask_httpauth import HTTPBasicAuth

from aidoc_app import app
import sys
sys.path.append('../')
import call_funcs

auth = HTTPBasicAuth()

users = {
    "admin": "admin"
}

@auth.get_password
def get_pw(username):
    if username in users:
        return users.get(username)
    return None

class VarsForm(Form):
    var1 = StringField('var1', validators=[DataRequired()])
    var2 = StringField('var2', validators=[DataRequired()])
    var3 = StringField('var3', validators=[DataRequired()])
    var4 = StringField('var4', validators=[DataRequired()])
    var5 = StringField('var5', validators=[DataRequired()])
    var6 = StringField('var6', validators=[DataRequired()])
    var7 = StringField('var7', validators=[DataRequired()])
    var8 = StringField('var8', validators=[DataRequired()])


# @app.route('/')
@app.route('/', methods=["GET"])
# @cache.cached(timeout=60)
@auth.login_required
def index(vars_=None, result=None, func_name=None):
    DEBUG_FLAG = True
    form = VarsForm()
    # print('asdsad: {}'.format(request.method))
    if vars_ is None and result is None:
        return render_template(
            'home.jade',
            name=auth.username(),
            form=form)
    else:
        return render_template(
            'home.jade',
            name=auth.username(),
            form=form,
            var1=vars_[0],
            var2=vars_[1],
            var3=vars_[2],
            var4=vars_[3],
            var5=vars_[4],
            var6=vars_[5],
            var7=vars_[6],
            var8=vars_[7],
            result=result,
            func_name=func_name
            )


@app.route('/result', methods=["POST"])
@auth.login_required
def result():
    try:
        var1 = float(request.form.get('var1'))
        var2 = float(request.form.get('var2'))
        var3 = float(request.form.get('var3'))
        var4 = float(request.form.get('var4'))
        var5 = float(request.form.get('var5'))
        var6 = float(request.form.get('var6'))
        var7 = float(request.form.get('var7'))
        var8 = float(request.form.get('var8'))
        l_vars = [var1, var2, var3, var4, var5, var6, var7, var8]
        to_calc = request.form.get('submit')
        if 'f1' in to_calc:
            func_name = 'f1'
            res = call_funcs.f1(l_vars)
        if 'f2' in to_calc:
            func_name = 'f2'
            res = call_funcs.f2(l_vars)
        if 'f3' in to_calc:
            func_name = 'f3'
            res = call_funcs.f3(l_vars)

        return index(vars_=l_vars, result=res, func_name=func_name)
    except Exception as exc:
        return render_template('error.jade')



    # return render_template('home.jade', name=auth.username(), form=form)

