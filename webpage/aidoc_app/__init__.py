"""
pythia_app initialization file
"""

from flask import Flask
from flask import render_template
from flask_cache import Cache
from livereload import Server

app = Flask(__name__, instance_relative_config=True, static_url_path='/static')
app.config.from_object('config')
app.config.from_pyfile('config.py')

app.jinja_env.add_extension('pyjade.ext.jinja.PyJadeExtension')
app.secret_key = 'key'
from aidoc_app.views import main

# if developing the webpage, run with livereload watcher
# server = Server(app.wsgi_app)
# server.serve()


# @app.route('/')
# @cache.cached(timeout=60)
# def index():
#     [...] # Make a few database calls to get the information we need
#     return render_template(
#         'index.html',
#         latest_posts=latest_posts,
#         recent_users=recent_users,
#         recent_photos=recent_photos
#     )

