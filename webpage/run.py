# export FLASK_APP=aidoc_app/__init__.py; export FLASK_DEBUG=true
# flask run

from setuptools import setup

setup(
    name='aidoc_app',
    packages=['aidoc_app'],
    include_package_data=True
)