import call_funcs





def main():
    # l = [1,2,3,4,5,6,7,8]
    l = [0,0,0,0,0,0,0,0]
    print(sum(x**2 for x in l))
    print(call_funcs.f0(l))

if __name__ == '__main__':
    main()