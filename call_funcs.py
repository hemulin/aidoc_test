import subprocess
import numpy as np
import os

def call_func(input, func_num, func_dir = '.'):
    func_file = '%s/f%d.out' %(func_dir, func_num)
    if len(input) == 0:
        return -1

    if isinstance(input[0], list) or isinstance(input[0], np.ndarray):
        cmd_args = ''
        for l in input:
            cmd_args += ' '.join(map(str, l)) + ' '
    else:
        cmd_args = ' '.join(map(str, input))

    # print(cmd_args)
    print(os.path.abspath(func_file))
    cmd_output = subprocess.getstatusoutput('%s %s' %(func_file, cmd_args))
    try:
        result = float(cmd_output[1])
        return result
    except:
        return cmd_output[1]



def f0(input):
    return call_func(input, 0)

def f1(input):
    return call_func(input, 1)

def f2(input):
    return call_func(input, 2)

def f3(input):
    return call_func(input, 3)
