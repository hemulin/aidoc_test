import numpy as np
import random
import call_funcs

def main():
    roi = get_min_region()
    min_vars, temp_output = init_min_vars(min_region=roi)
    print("min_vars: {}, output: {}".format(min_vars, temp_output))
    # assume non dependent variables
    # final_vars = []
    for i in range(len(min_vars)):
        comp_min = find_single_comp_min(i, min_vars, roi)
        # final_vars.append(comp_min)
        min_vars[i] = comp_min # updating the vector
        # print('comp_idx: {}, val: {}'.format(i, comp_min))
    print("min_vars = {}, func_res = {}".format(min_vars, call_funcs.f1(min_vars)))
    # print("final_vars = {}, func_res = {}".format(min_vars, call_funcs.f1(final_vars)))

def find_single_comp_min(play_idx, min_vars, region_pow):
    temp_vars = min_vars[:]
    temp_comp = temp_vars[play_idx]
    temp_res = -float('Inf')
    final_res = float('Inf')
    epsilon = 0.1
    old_res = 0
    # jump = abs(10**region_pow - min_vars[play_idx])
    jump = 10**region_pow / 2.0
    # jump = min_vars[play_idx]
    while abs(old_res - final_res) > epsilon:
        jump = jump / 2
        old_res = call_funcs.f1(temp_vars)
        current_comp_val = temp_vars[play_idx]
        temp_vars[play_idx] = current_comp_val + jump
        new_res = call_funcs.f1(temp_vars)
        if new_res < old_res:
            final_res = new_res
        else:
            temp_vars[play_idx] = current_comp_val - jump
            final_res = call_funcs.f1(temp_vars)
        temp_comp = temp_vars[play_idx]
    return temp_comp


def get_min_region():
    min_i = 0
    temp_res = float('Inf')
    for i in range(1,7):
        current_vars = np.random.uniform(-10**i, 10**i, 8)
        func_res = call_funcs.f1(current_vars)
        if func_res < temp_res:
            temp_res = func_res
            min_i = i
    return min_i

def init_min_vars(num_of_runs=10**3, min_region=2):
    temp_res = float('Inf')
    min_vars = []
    for i in range(10000):
        current_vars = np.random.uniform(-10**min_region, 10**min_region, 8)
        func_res = call_funcs.f1(current_vars)
        if func_res < temp_res:
            min_vars = current_vars
    return min_vars, call_funcs.f1(min_vars)

if __name__ == '__main__':
    main()